jQuery( window ).on( 'elementor/frontend/init', () => {

   const addHandler = ( $element ) => {
       $('body').find('.pix-shape-dividers').each(function(){
            if(!$(this).hasClass('loaded')){
                let divider = new dividerShapes(this);
                divider.initPoints();
                $(this).addClass('loaded');
            }
        });
   };

   elementorFrontend.hooks.addAction( 'frontend/element_ready/pix-dividers.default', addHandler );
} );
