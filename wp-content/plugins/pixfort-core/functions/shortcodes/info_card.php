<?php

/* ---------------------------------------------------------------------------
  * Info_card [Info_card]
  * --------------------------------------------------------------------------- */
 if( ! function_exists( 'sc_info_card' ) )
 {
    function sc_info_card( $attr, $content = null )
    {
        extract(shortcode_atts(array(
            'title' 		=> '',
            'text' 		=> '',
            'bg_img' 		=> '',
            'position'      => 0
        ), $attr));

        if( $bg_img ) {
			$img = wp_get_attachment_image_src($bg_img, "large");
			$imgSrc = $img[0];
			//$thumb_image_url = aq_resize( $imgSrc, 148, 160, true, true, true );
		}
        $output = '';
        if($position==0){
            $output .= '<div class="card mb-3 pix-info-card card-shadow-lg-inverse">
                 <div class="card-inner">
                        <a href="#">
                           <img class="card-img animating fade-in-Img" src="'.$imgSrc.'" alt="Card image">
                           <div class="card-img-overlay p-0 animating fade-in-down">
                               <div class="card-content-box p-3">
                                   <h6 class="card-text text-secondary animating slide-in-up">'.$text.'</h6>
                                   <h2 class="card-title animating fade-in-left"><strong>'.$title.'</strong></h2>
                               </div>
                           </div>
                       </a>
                   </div>
             </div>';
        }else{
            $output .= '<div class="card mb-3 pix-info-card card-shadow-lg-inverse">
            <div class="card-inner">
               <a href="#">
                  <div class="card-img-overlay p-0 d-flex flex-column justify-content-end animating fade-in-up">
                      <div class="card-content-box p-3">
                          <h6 class="card-text text-secondary animating slide-in-up">'.$text.'</h6>
                          <h2 class="card-title animating fade-in-left"><strong>'.$title.'</strong></h2>
                      </div>
                  </div>
                  <img class="card-img-bottom animating fade-in-Img" src="'.$imgSrc.'" alt="Card image">
              </a>
          </div>
        </div>';
        }

        return $output;
    }
 }

 add_shortcode('info_card', 'sc_info_card');

  ?>
