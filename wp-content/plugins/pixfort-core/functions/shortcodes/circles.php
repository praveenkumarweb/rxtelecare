<?php
/* ---------------------------------------------------------------------------
 * Circles [circles] [/circles]
* --------------------------------------------------------------------------- */
if( ! function_exists( 'sc_circles' ) )
{
	function sc_circles( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'circles'		=> '',
			'effect'		=> '',
			'hover_effect'		=> '',
			'btn_text'		=> '',
			'btn_link'		=> '',
			'circles_size'		=> 'pix-sm-circles',
			'circles_align'		=> '',
			'c_css' 		=> '',
			'css' 		=> '',
		), $attr));
		$attr['btn_mb'] = 'mb-0';
		$attr['btn_animation'] = 'fade-in-left';
		$attr['btn_anim_delay'] = '1300';

		$circles_arr = array();
		$css_class = '';

		$elementor = false;
		if(is_array($circles)){
			$circles_arr = $circles;
			$elementor = true;
		}else{
			if(function_exists('vc_param_group_parse_atts')){
				$circles_arr = vc_param_group_parse_atts( $circles );
			}
		}


		$output = '';

        $effect_arr = array(
           "" => "",
           "1"       => "shadow-sm",
           "2"       => "shadow",
           "3"       => "shadow-lg",
           "4"       => "shadow-inverse-sm",
           "5"       => "shadow-inverse",
           "6"       => "shadow-inverse-lg",
         );

         $hover_effect_arr = array(
            ""       => "",
            "1"       => "shadow-hover-sm",
            "2"       => "shadow-hover",
            "3"       => "shadow-hover-lg",
            "4"       => "shadow-inverse-hover-sm",
            "5"       => "shadow-inverse-hover",
            "6"       => "shadow-inverse-hover-lg",
         );
		 if(function_exists('vc_shortcode_custom_css_class')){
			 $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $c_css, ' ' ) );
		 }

         $classes = ' ';
         if($effect){ $classes .= $effect_arr[$effect]. ' '; }
         if($hover_effect){ $classes .= $hover_effect_arr[$hover_effect]. ' '; }
		$i = 1000;
		$circles_html = '';

		foreach ($circles_arr as $key => $value) {
            if( !empty($value["img"]) ) {
                // $img = wp_get_attachment_image_src($value["img"], "full");
				$imgSrcset = '';
				$imgSizes = '';
				if(is_string($value["img"])&&substr( $value["img"], 0, 4 ) === "http"){
					$img = $value["img"];
					$imgSrc = $img;
				}else{
					if(!empty($value["img"]['id'])){
					  $img = wp_get_attachment_image_src($value["img"]['id'], "pix-woocommerce-md");
					  $imgSrcset = wp_get_attachment_image_srcset($value["img"]['id']);
					  if(!empty($imgSrcset)){
						  $imgSrcset = 'srcset="'.$imgSrcset.'"';
						  $imgSizes = 'sizes="'.wp_get_attachment_image_sizes($value["img"]['id'], "full").'"';
					  }

					  $imgSrc = '';
					  if(is_array($img)){
						  $imgSrc = $img[0];
					  }
					  if(!$img&&$value["img"]['url']){
	                      $imgSrc = $value["img"]['url'];
	                  }
					}else{
					  $img = wp_get_attachment_image_src($value["img"], "pix-woocommerce-md");
					  $imgSrcset = wp_get_attachment_image_srcset($value["img"]);
					  if(!empty($imgSrcset)){
						  $imgSrcset = 'srcset="'.$imgSrcset.'"';
						  $imgSizes = 'sizes="'.wp_get_attachment_image_sizes($value["img"], "full").'"';
					  }

					  $imgSrc = $img[0];
					}

				}
    		}
			if(empty($value['color'])) $value['color']='';
			// if(empty($value['link'])) $value['link']='';

            $color = $value['color'];
			$title = empty($value["title"])? '':$value["title"];
			if(empty($value['link'])){
				$circles_html .= '<span class="align-middle circle-item pix-mr-5 '.$color . $classes .' animate-in" data-anim-type="fade-in-left" data-anim-delay="'. $i .'" data-toggle="tooltip" data-placement="bottom" title="'. $title .'"><img src="'. $imgSrc .'" '.$imgSrcset.' '.$imgSizes.' class="rounded-circle bg-white" loading="lazy" alt="" /></span>';
			}else{
				$circles_html .= '<a class="align-middle circle-item pix-mr-5 '.$color . $classes .' animate-in" data-anim-type="fade-in-left" data-anim-delay="'. $i .'"  href="'. $value["link"] .'" data-toggle="tooltip" data-placement="bottom" title="'. $title .'"><img src="'. $imgSrc .'" '.$imgSrcset.' '.$imgSizes.' class="rounded-circle bg-white" loading="lazy"  alt="" /></a>';
			}
			$i+=100;
		}


		// $output = '<div class="d-flex '.esc_attr( $css_class ).'">';
		$output = '<div class="d-inline-block2 d-sm-flex align-items-center '.$circles_align.'  '.esc_attr( $css_class ).'">';
			$output .= '<div class="'.$circles_size.' d-inline-block2 d-flex align-items-center align-self-center align-middle">'. $circles_html .'</div>';
			if(!empty($btn_text)){
				$output .= '<div class="d-inline-block2 pix-ml-5 align-self-center align-items-center align-items-center d-flex pt-md-0 pt-3 justify-content-center">';
					// $output .= '<a href="'.$btn_link.'" class="btn btn-link btn-md text-dark animate-in" data-anim-type="fade-in-left" data-anim-delay="1300">Check all categories <i class="pixicon-angle-right"></i></a>';
					$output .= sc_pix_button($attr);
		        $output .= '</div>';
			}
        $output .= '</div>';

		// <div class="d-flex">
        //     <div class="pix-sm-circles lign-self-center">
        //         <a class="circle-item pix-bg-custom animate-in" data-anim-type="fade-in-left" data-anim-delay="1000" href="/demo-startup.html#element/stories" data-toggle="tooltip" data-placement="bottom" title="Secure Platform"><img src="images/startup/story-1.png" class="rounded-circle" /></a>
        //         <a class="circle-item pix-bg-custom animate-in" data-anim-type="fade-in-left" data-anim-delay="1100" href="/demo-startup.html#element/stories" data-toggle="tooltip" data-placement="bottom" title="Worldwide"><img src="images/startup/story-2.png" class="rounded-circle" /></a>
        //         <a class="circle-item pix-bg-custom animate-in" data-anim-type="fade-in-left" data-anim-delay="1200" href="/demo-startup.html#element/stories" data-toggle="tooltip" data-placement="bottom" title="Online Shop"><img src="images/startup/story-3.png" class="rounded-circle" /></a>
        //     </div>
        //     <div class="align-self-center">
        //         <a class="btn btn-link btn-md text-dark animate-in" data-anim-type="fade-in-left" data-anim-delay="1300">Check all categories <i class="pixicon-angle-right"></i></a>
        //     </div>
        // </div>


		return $output;
	}
}
add_shortcode( 'circles', 'sc_circles' );
