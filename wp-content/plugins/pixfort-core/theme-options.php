<?php



$colors = array(
    "Transparent"			=> "transparent",
    "Primary"				=> "primary",
    "Primary Gradient"		=> "gradient-primary",
    "Secondary"				=> "secondary",
    "Primary Gradient"		=> "gradient-primary",
    "White"					=> "white",
    "Black"					=> "black",
    "Green"					=> "green",
    "Blue"					=> "blue",
    "Red"					=> "red",
    "Yellow"				=> "yellow",
    "Brown"					=> "brown",
    "Purple"				=> "purple",
    "Orange"				=> "orange",
    "Cyan"					=> "cyan",
    "Gray 1"				=> "gray-1",
    "Gray 2"				=> "gray-2",
    "Gray 3"				=> "gray-3",
    "Gray 4"				=> "gray-4",
    "Gray 5"				=> "gray-5",
    "Gray 6"				=> "gray-6",
    "Gray 7"				=> "gray-7",
    "Gray 8"				=> "gray-8",
    "Gray 9"				=> "gray-9",
    "Dark opacity 1"		=> "dark-opacity-1",
    "Dark opacity 2"		=> "dark-opacity-2",
    "Dark opacity 3"		=> "dark-opacity-3",
    "Dark opacity 4"		=> "dark-opacity-4",
    "Dark opacity 5"		=> "dark-opacity-5",
    "Dark opacity 6"		=> "dark-opacity-6",
    "Dark opacity 7"		=> "dark-opacity-7",
    "Dark opacity 8"		=> "dark-opacity-8",
    "Dark opacity 9"		=> "dark-opacity-9",
    "Light opacity 1"		=> "light-opacity-1",
    "Light opacity 2"		=> "light-opacity-2",
    "Light opacity 3"		=> "light-opacity-3",
    "Light opacity 4"		=> "light-opacity-4",
    "Light opacity 5"		=> "light-opacity-5",
    "Light opacity 6"		=> "light-opacity-6",
    "Light opacity 7"		=> "light-opacity-7",
    "Light opacity 8"		=> "light-opacity-8",
    "Light opacity 9"		=> "light-opacity-9",
    "Custom"				=> "custom"
);

$bg_colors = array(
    "Transparent"			=> "transparent",
    "Primary"				=> "primary",
    "Primary Light"			=> "primary-light",
    "Primary Gradient"		=> "gradient-primary",
    "Primary Gradient Light"		=> "gradient-primary-light",
    "Secondary"				=> "secondary",
    "Secondary Light"		=> "secondary-light",
    "White"					=> "white",
    "Black"					=> "black",
    "Green"					=> "green",
    "Green Light"			=> "green-light",
    "Blue"					=> "blue",
    "Blue Light"			=> "blue-light",
    "Red"					=> "red",
    "Red Light"				=> "red-light",
    "Yellow"				=> "yellow",
    "Yellow Light"			=> "yellow-light",
    "Brown"					=> "brown",
    "Brown Light"			=> "brown-light",
    "Purple"				=> "purple",
    "Purple Light"			=> "purple-light",
    "Orange"				=> "orange",
    "Orange Light"			=> "orange-light",
    "Cyan"					=> "cyan",
    "Cyan Light"			=> "cyan-light",
    "Gray 1"				=> "gray-1",
    "Gray 2"				=> "gray-2",
    "Gray 3"				=> "gray-3",
    "Gray 4"				=> "gray-4",
    "Gray 5"				=> "gray-5",
    "Gray 6"				=> "gray-6",
    "Gray 7"				=> "gray-7",
    "Gray 8"				=> "gray-8",
    "Gray 9"				=> "gray-9",
    "Dark opacity 1"		=> "dark-opacity-1",
    "Dark opacity 2"		=> "dark-opacity-2",
    "Dark opacity 3"		=> "dark-opacity-3",
    "Dark opacity 4"		=> "dark-opacity-4",
    "Dark opacity 5"		=> "dark-opacity-5",
    "Dark opacity 6"		=> "dark-opacity-6",
    "Dark opacity 7"		=> "dark-opacity-7",
    "Dark opacity 8"		=> "dark-opacity-8",
    "Dark opacity 9"		=> "dark-opacity-9",
    "Light opacity 1"		=> "light-opacity-1",
    "Light opacity 2"		=> "light-opacity-2",
    "Light opacity 3"		=> "light-opacity-3",
    "Light opacity 4"		=> "light-opacity-4",
    "Light opacity 5"		=> "light-opacity-5",
    "Light opacity 6"		=> "light-opacity-6",
    "Light opacity 7"		=> "light-opacity-7",
    "Light opacity 8"		=> "light-opacity-8",
    "Light opacity 9"		=> "light-opacity-9",
    "Custom"				=> "custom"
);

$footer_posts = get_posts([
    'post_type' => 'pixfooter',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$footers = array();
$footers[''] = "Disabled";
foreach ($footer_posts as $key => $value) {
    $footers[$value->ID] = $value->post_title;
}

$popup_posts = get_posts([
    'post_type' => 'pixpopup',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$popups = array();
$popups[''] = "Disabled";
foreach ($popup_posts as $key => $value) {
    $popups[$value->ID] = $value->post_title;
}


$pages_posts = get_posts([
    'post_type' => 'page',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$pages = array();
$pages[''] = "Disabled";
foreach ($pages_posts as $key => $value) {
    $pages[$value->ID] = $value->post_title;
}


$header_posts = get_posts([
    'post_type' => 'pixheader',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$headers = array();

$headers['default'] = "Default";
$headers[''] = "Disable";
foreach ($header_posts as $key => $value) {
    $headers[$value->ID] = $value->post_title;
}

$sidebars = array();
$sidebars['sidebar-1'] = 'Main Sidebar';
if(!empty(pix_plugin_get_option('pix_sidebars'))){
    foreach (pix_plugin_get_option('pix_sidebars') as $key => $value) {
        $sidebars['sidebar-'.str_replace(' ', '', $value)] = $value;
    }
    // $sidebars = array_merge($sidebars,);
}


Redux::setSection( $opt_name, array(
    'title' => __( 'General Settings', 'redux-framework-demo' ),
    'id'    => 'general',
    'desc'  => __( 'Basic fields as subsections.', 'redux-framework-demo' ),
    // 'icon'  => 'el el-home',
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/home.svg',
    'icon_type'  => 'svg'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    'id'         => 'general-settings',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'logo-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Custom Logo', 'redux-framework-demo'),
            // 'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
            // 'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
        ),
        array(
            'id'       => 'retina-logo-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Retina Logo', 'redux-framework-demo'),
            'desc'     => __('Retina Logo should be 2x larger than Custom Logo (field is optional).', 'redux-framework-demo'),
            // 'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
        ),
        array(
            'id'       => 'scroll-logo-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Scroll Logo', 'redux-framework-demo'),
            'desc'     => __('Scroll Logo replaces the default logo when scrolling with the transparent header.', 'redux-framework-demo'),
            // 'required' => array('opt-header-layout','=','transparent'),
        ),

        array(
            'id'       => 'mobile-logo-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Mobile Logo', 'redux-framework-demo'),
            'desc'     => __('Mobile Logo is display in mobile devices only.', 'redux-framework-demo'),
            // 'required' => array('opt-header-layout','=','transparent'),
        ),


        array(
            'id'       => 'favicon-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Custom Favicon', 'redux-framework-demo'),
            'desc'     => __('Add a small image to be displayed in browser tabs.', 'redux-framework-demo'),
            'subtitle' => __('Site favicon', 'redux-framework-demo'),
        ),


        array(
            'id'       => 'pix-body-padding',
            'type'     => 'select',
            'title'    => __('Page padding', 'redux-framework-demo'),
            'desc'     => __('Add padding around the page.', 'redux-framework-demo'),
            'options'  => array(
                ''             => 'None',
                'pix-p-5'         => '5px',
                'pix-p-10'         => '10px',
                'pix-p-15'         => '15px',
                'pix-p-20'         => '20px',
                'pix-p-25'         => '25px',
                'pix-p-30'         => '30px',
                'pix-p-35'         => '35px',
                'pix-p-40'         => '40px',
                'pix-p-45'         => '45px',
                'pix-p-50'         => '50px',
            ),
            'default'  => '',
        ),

        array(
            'id'       => 'pix-body-bg-color',
            'type'     => 'select',
            'title'    => __('Background color', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'required' => array('pix-body-padding','!=',''),
        ),
        array(
            'id'       => 'custom-body-bg-color',
            'type'     => 'color',
            'title'    => __('Custom background Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('pix-body-bg-color','equals','custom'),
            'validate' => 'color',
        ),




        array(
            'id'       => 'website-preview',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Website image preview', 'redux-framework-demo'),
            'desc'     => __('Use a custom image to display when the website is shared on social media.', 'redux-framework-demo'),
            // 'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
        ),


        array(
            'id'       => 'back-to-top',
            'type'     => 'select',
            'title'    => __('Back to top button', 'redux-framework-demo'),
            // 'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'default'   => 'default',
            // 'required' => array('show-banner-btn','!=',false),
            'options'  => array(
                "default"            => "Default (bottom right)",
                "is-left"        => "Bottom left",
                "disable"        => "Disable"
            ),
        ),

    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Popups', 'redux-framework-demo' ),
    'id'         => 'exit_popup',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'pix-exit-popup',
            'type'     => 'select',
            'title'    => __('Exit popup', 'redux-framework-demo'),
            'desc' => __('The popup will show when the mouse leave the browser tab.', 'redux-framework-demo'),
            'options'  => $popups,
        ),
        array(
            'id'       => 'pix-exit-popup-id',
            'type'     => 'text',
            'title'    => __( 'Exit popup ID', 'redux-framework-demo' ),
            'desc' => __('Changing the ID will reset the closed state for all users (all users will start to see the get again).', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'exit-popup-1',
            'required' => array('pix-exit-popup','!=',false),
        ),

        array(
            'id'       => 'pix-automatic-popup',
            'type'     => 'select',
            'title'    => __('Automatic popup', 'redux-framework-demo'),
            'desc' => __('The popup will show after a specified amount of time.', 'redux-framework-demo'),
            'options'  => $popups,
        ),

        array(
            'id'       => 'pix-automatic-popup-id',
            'type'     => 'text',
            'title'    => __( 'automatic popup ID', 'redux-framework-demo' ),
            'desc' => __('Changing the ID will reset the closed state for all users (all users will start to see the get again).', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'automatic-popup-1',
            'required' => array('pix-automatic-popup','!=',false),
        ),

        array(
            'id'       => 'pix-automatic-popup-time',
            'type'     => 'text',
            'title'    => __( 'Automatic popup time', 'redux-framework-demo' ),
            'desc' => __('The time before opening the popup (in seconds).', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => '5',
            'required' => array('pix-automatic-popup','!=',''),
        ),
    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Sidebars', 'redux-framework-demo' ),
    'id'         => 'sidebars',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'=>'pix_sidebars',
            'type' => 'multi_text',
            'title' => __('Sidebars', 'redux-framework-demo'),
            'subtitle' => __('Manage custom sidebars', 'redux-framework-demo'),
            'desc' => __('Sidebars can be used on pages, blog and portfolio.', 'redux-framework-demo')
        ),
    )
) );


$opts_dividers = array();
$opts_dividers[0] = array(
    'img'   => PIX_CORE_PLUGIN_URI.'functions/images/shapes/none.png'
);
for ($x = 1; $x <= 23; $x++) {
    $opts_dividers[$x] = array(
        'img'   => PIX_CORE_PLUGIN_URI.'functions/images/shapes/divider-'.$x.'.png'
    );
}











Redux::setSection( $opt_name, array(
    'title'      => __( 'API Keys', 'redux-framework-demo' ),
    'id'         => 'api_keys',
    'desc'       => __( 'For detailed information about setting up Google maps check this article from our knowledge base: ', 'redux-framework-demo' ) . '<a target="_blank" href="https://essentials.pixfort.com/knowledge-base/using-advanced-google-maps-styles/" target="_blank">https://essentials.pixfort.com/knowledge-base/using-advanced-google-maps-styles/</a>',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'google-api-key',
            'type'     => 'text',
            'title'    => __( 'Google API Key', 'redux-framework-demo' ),
            'desc'  => __( 'Google API Key is required for Google Maps elements.', 'redux-framework-demo' ),
        ),

    )
) );





Redux::setSection( $opt_name, array(
    'title'      => __( 'Cookies consent', 'redux-framework-demo' ),
    'id'         => 'cookies_consent',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'pix-enable-cookies',
            'type'     => 'switch',
            'title'    => __('Enable Cookies banner', 'redux-framework-demo'),
            'desc' => __('Add Cookies consent bar at the bottom of the page.', 'redux-framework-demo'),
            'default'  => false,
        ),

        array(
            'id'       => 'pix-cookies-id',
            'type'     => 'text',
            'title'    => __( 'Cookies ID', 'redux-framework-demo' ),
            'desc' => __('Changing the ID will reset the closed state for all users (all users will start to see the banner again).', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'Cookies-1',
            'required' => array('pix-enable-cookies','!=',false),
        ),
        array(
            'id'       => 'pix-cookies-text',
            'type'     => 'text',
            'title'    => __( 'Banner text', 'redux-framework-demo' ),
            'default'    => 'By using this website, you agree to our',
            // 'desc'  => __( 'By using this website, you agree to our', 'redux-framework-demo' ),
            'required' => array('pix-enable-cookies','=',true),
        ),
        array(
            'id'       => 'pix-cookies-btn',
            'type'     => 'text',
            'title'    => __( 'Banner Button text', 'redux-framework-demo' ),
            'default'  => 'cookie policy.',
            'required' => array('pix-enable-cookies','=',true),
        ),
        array(
            'id'       => 'pix-cookies-page',
            'type'     => 'select',
            'title'    => __('Cookie policy page', 'redux-framework-demo'),
            'options'  => $pages,
            'required' => array('pix-enable-cookies','=',true),
        ),
        array(
            'id'       => 'pix-cookies-url',
            'type'     => 'text',
            'title'    => __( 'Cookies policy URL', 'redux-framework-demo' ),
            'desc'  => __( 'Show the ppolicy in a link (optional).', 'redux-framework-demo' ),
        ),

        array(
            'id'       => 'pix-cookies-target',
            'type'     => 'switch',
            'required' => array('pix-cookies-url','!=',''),
            'title'    => __('Open in a new tab', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'pix-cookies-popup',
            'type'     => 'select',
            'title'    => __('Cookie policy popup', 'redux-framework-demo'),
            'options'  => $popups,
            'required' => array('pix-enable-cookies','=',true),
            'desc' => __('Show the policy in a popup instead of redirecting the user to a page.', 'redux-framework-demo'),
        ),



        array(
            'id'       => 'cookie-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Cookie banner image', 'redux-framework-demo'),
            'desc'     => __('Image will be displayed at 30 pixels size.', 'redux-framework-demo'),
            'subtitle' => __('Leave empty to display the deafult image icon', 'redux-framework-demo'),
        ),
    )
) );




Redux::setSection( $opt_name, array(
    'title'      => __( 'Elementor', 'redux-framework-demo' ),
    'id'         => 'pix_elementor',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'pix-disable-elementor-demo',
            'type'     => 'switch',
            'title'    => __('Disable pixfort demo blocks', 'redux-framework-demo'),
            'desc' => __('If you disable pixfort demo block from Elementor library you will see the default Elementor blocks instead.', 'redux-framework-demo'),
            'default'  => false,
        ),


    )
) );






Redux::setSection( $opt_name, array(
    'title' => __( 'Layout', 'redux-framework-demo' ),
    'id'    => 'layout',
    'desc'  => __( 'Basic fields as subsections.', 'redux-framework-demo' ),
    // 'icon'  => 'el el-home'
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/layout.svg',
    'icon_type'  => 'svg'
) );

// Redux::setSection( $opt_name, array(
//     'title'      => __( 'General', 'redux-framework-demo' ),
//     'id'         => 'layout-general',
//     'subsection' => true,
//     'fields'     => array(
//
//         array(
//             'id'     => 'section-end',
//             'type'   => 'section',
//             'indent' => false,
//         ),
//
//         array(
//             'id'       => 'footer-call-to-action',
//             'type'     => 'text',
//             'title'    => __( 'Footer Call To Action', 'redux-framework-demo' ),
//         ),
//         array(
//             'id'       => 'footer-call-button',
//             'type'     => 'text',
//             'title'    => __( 'Footer Call Button text', 'redux-framework-demo' ),
//         ),
//         array(
//             'id'       => 'footer-call-link',
//             'type'     => 'text',
//             'title'    => __( 'Footer Call link', 'redux-framework-demo' ),
//         ),
//         array(
//             'id'       => 'footer-copy',
//             'type'     => 'text',
//             'title'    => __( 'Footer Copyright', 'redux-framework-demo' ),
//             'desc'     => __( 'Leave this field blank to show a default copyright.', 'redux-framework-demo' ),
//         ),
//     )
// ) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Header', 'redux-framework-demo' ),
    'id'         => 'headers',
    'subsection' => true,
    'desc'       => __( 'For detailed information about setting up the header check this article from our knowledge base: ', 'redux-framework-demo' ) . '<a target="_blank" href="https://essentials.pixfort.com/knowledge-base/creating-website-header" target="_blank">https://essentials.pixfort.com/knowledge-base/creating-website-header</a>',
    'fields'     => array(


        array(
            'id'       => 'pix-header',
            'type'     => 'select',
            'title'    => __('Website header', 'redux-framework-demo'),
            'default'   => 'default',
            'options'  => $headers,
        ),





    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Footer', 'redux-framework-demo' ),
    'id'         => 'footers',
    'subsection' => true,
    'desc'       => __( 'For detailed information about setting up the footer check this article from our knowledge base: ', 'redux-framework-demo' ) . '<a target="_blank" href="https://essentials.pixfort.com/knowledge-base/creating-website-footer" target="_blank">https://essentials.pixfort.com/knowledge-base/creating-website-footer</a>',
    'fields'     => array(


        array(
            'id'       => 'pix-footer',
            'type'     => 'select',
            'title'    => __('Footer', 'redux-framework-demo'),
            'options'  => $footers,
        ),

        array(
            'id'       => 'pix-sticky-footer',
            'type'     => 'switch',
            'title'    => __('Enable sticky footer', 'redux-framework-demo'),
            'default'  => false,
        ),

        array(
            'id'       => 'sticky-footer-bg-color',
            'type'     => 'select',
            'title'    => __('Sticky footer fade color', 'redux-framework-demo'),
            'options'  => array_flip($bg_colors),
            'default'  => 'gradient-primary',
        ),
        array(
            'id'       => 'custom-sticky-footer-bg-color',
            'type'     => 'color',
            'title'    => __('Custom Sticky footer fade color', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('sticky-footer-bg-color','equals','custom'),
            'validate' => 'color',
        ),




    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'banner', 'redux-framework-demo' ),
    'id'         => 'layout-banner',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'show-banner',
            'type'     => 'switch',
            'title'    => __('Show Banner', 'redux-framework-demo'),
            'desc' => __('Show the banner on top of the page', 'redux-framework-demo'),
            'default'  => false,
        ),

        array(
            'id'       => 'banner-id',
            'type'     => 'text',
            'title'    => __( 'Banner ID', 'redux-framework-demo' ),
            'desc' => __('Changing the ID will reset the closed state for all users (all users will start to see the banner again).', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'Banner-1',
            'required' => array('show-banner','!=',false),
        ),
        array(
            'id'       => 'banner-text',
            'type'     => 'text',
            'title'    => __( 'Banner Text', 'redux-framework-demo' ),
            // 'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => '',
            'required' => array('show-banner','!=',false),
        ),

        array(
            'id'       => 'banner-bg',
            'type'     => 'select',
            'title'    => __('Background color', 'redux-framework-demo'),
            // 'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array_flip($bg_colors),
            'required' => array('show-banner','!=',false),
            // 'default'  => '2',
        ),



        array(
            'id'       => 'custom-banner-bg',
            'type'     => 'color',
            'title'    => __('Custom banner Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('top-bar-bg','equals','custom'),
            'validate' => 'color',
        ),

        array(
            'id'       => 'banner-bg-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('background image', 'redux-framework-demo'),
            'required' => array('show-banner','!=',false),
        ),



        array(
            'id'       => 'bold-banner-text',
            'type'     => 'switch',
            'title'    => __('Bold topbar text', 'redux-framework-demo'),
            'default'  => true,
            'required' => array('show-banner','!=',false),
        ),
        array(
            'id'       => 'secondary-banner-text',
            'type'     => 'switch',
            'title'    => __('Use secondary font for Topbar text', 'redux-framework-demo'),
            'default'  => false,
            'required' => array('show-banner','!=',false),
        ),

        array(
            'id'       => 'banner-text-color',
            'type'     => 'select',
            'title'    => __('Text Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'gray-9',
            'required' => array('show-banner','!=',false),

        ),

        array(
            'id'       => 'banner-custom-text-color',
            'type'     => 'color',
            'title'    => __('Custom banner text color', 'redux-framework-demo'),
            'default'  => '#212529',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('banner-text-color','=','custom'),
        ),


        array(
            'id'       => 'show-banner-btn',
            'type'     => 'switch',
            'title'    => __('Show Banner button', 'redux-framework-demo'),
            // 'desc' => __('Show banner button', 'redux-framework-demo'),
            'default'  => false,
            'required' => array('show-banner','!=',false),
        ),


        array(
            'id'       => 'banner-btn-text',
            'type'     => 'text',
            'title'    => __( 'Banner Text', 'redux-framework-demo' ),
            // 'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'Check it Now',
            'required' => array('show-banner-btn','!=',false),
        ),
        array(
            'id'       => 'banner-btn-link',
            'type'     => 'text',
            'title'    => __( 'Banner Link', 'redux-framework-demo' ),
            'default'  => '#',
            'required' => array('show-banner-btn','!=',false),
        ),

        array(
            'id'       => 'show-banner-target',
            'type'     => 'switch',
            'required' => array('show-banner-btn','!=',false),
            'title'    => __('Open in a new tab', 'redux-framework-demo'),
            'default'  => true,
        ),


        array(
            'id'       => 'banner-btn-style',
            'type'     => 'select',
            'title'    => __('Button style', 'redux-framework-demo'),
            // 'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'default'   => '',
            'required' => array('show-banner-btn','!=',false),
            'options'  => array(
                ""            => "Default",
                "flat"        => "Flat",
                "line"        => "Line",
                "outline"     => "Outline",
                "underline"     => "Underline",
                "blink"     => "Blink"
            ),
        ),

        array(
            'id'       => 'banner-btn-color',
            'type'     => 'select',
            'required' => array('show-banner-btn','!=',false),
            'title'    => __('Button color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'default'   => 'primary',
            'options'  => array(
                'primary' 		=> 'Primary',
                'primary-light' 		=> 'Primary Light',
                // 'success'		=> 'Success',
                'secondary'		=> 'Secondary',
                'light' 		=> 'Light',
                'dark' 		    => 'Dark',
                'black' 		=> 'Black',
                'link' 		    => 'Link',
                'white' 		=> 'White',
                'blue' 		    => 'Blue',
                'red' 		    => 'Red',
                'cyan' 		    => 'Cyan',
                'orange' 		    => 'Orange',
                'green' 		    => 'Green',
                'purple' 		    => 'Purple',
                'brown' 		    => 'Brown',
                'yellow' 		    => 'Yellow',
                'bg-gradient-primary' 		    => 'Primary gradient',
                "gray-1" => 'Gray 1',
                "gray-2" => 'Gray 2',
                "gray-3" => 'Gray 3',
                "gray-4" => 'Gray 4',
                "gray-5" => 'Gray 5',
                "gray-6" => 'Gray 6',
                "gray-7" => 'Gray 7',
                "gray-8" => 'Gray 8',
                "gray-9" => 'Gray 9',
                "bg-dark-opacity-1" => 'Dark opacity 1',
                "bg-dark-opacity-2" => 'Dark opacity 2',
                "bg-dark-opacity-3" => 'Dark opacity 3',
                "bg-dark-opacity-4" => 'Dark opacity 4',
                "bg-dark-opacity-5" => 'Dark opacity 5',
                "bg-dark-opacity-6" => 'Dark opacity 6',
                "bg-dark-opacity-7" => 'Dark opacity 7',
                "bg-dark-opacity-8" => 'Dark opacity 8',
                "bg-dark-opacity-9" => 'Dark opacity 9',
                "bg-light-opacity-1" => 'Light opacity 1',
                "bg-light-opacity-2" => 'Light opacity 2',
                "bg-light-opacity-3" => 'Light opacity 3',
                "bg-light-opacity-4" => 'Light opacity 4',
                "bg-light-opacity-5" => 'Light opacity 5',
                "bg-light-opacity-6" => 'Light opacity 6',
                "bg-light-opacity-7" => 'Light opacity 7',
                "bg-light-opacity-8" => 'Light opacity 8',
                "bg-light-opacity-9" => 'Light opacity 9'

            ),

        ),

        array(
            'id'       => 'banner-btn-text-color',
            'type'     => 'select',
            'required' => array('show-banner-btn','!=',false),
            'title'    => __('Text Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => '',

        ),

        array(
            'id'       => 'banner-btn-custom-text-color',
            'type'     => 'color',
            'title'    => __('Custom banner text color', 'redux-framework-demo'),
            'default'  => '#212529',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('banner-btn-text-color','=','custom'),
        ),


        // array(
        //     'id'          => 'banner-date',
        //     'type'        => 'date',
        //     'title'       => __('Date Option', 'redux-framework-demo'),
        //     'subtitle'    => __('No validation can be done on this field type', 'redux-framework-demo'),
        //     'desc'        => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
        //     'placeholder' => 'Click to enter a date'
        // ),

        array(
            'id'       => 'show-banner-countdown',
            'type'     => 'switch',
            'required' => array('show-banner','!=',false),
            'title'    => __('Show countdown', 'redux-framework-demo'),
            'default'  => false,
        ),

        array(
            'id'       => 'banner-date',
            'type'     => 'text',
            'title'    => __( 'Banner Countdown Date', 'redux-framework-demo' ),
            'default'  => '2020/10/10T00:48',
            'required' => array('show-banner-countdown','!=',false),
            'desc' => __('Example: 2021/12/30 12:00', 'redux-framework-demo'),
        ),

        array(
            'id'       => 'banner-padding',
            'type'     => 'select',
            'title'    => __('Banner padding', 'redux-framework-demo'),
            // 'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'default'   => '',
            'options'  => array(
                ""            => "Default",
                "pix-py-5"        => "5px",
                "pix-py-10"        => "10px",
                "pix-py-20"        => "20px",
            ),
            'required' => array('show-banner','!=',false),
        ),

    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Search', 'redux-framework-demo' ),
    'id'         => 'layout-search',
    'subsection' => true,
    'fields'     => array(


        array(
            'id'       => 'search-style',
            'type'     => 'button_set',
            'title'    => __('Search overlay style', 'redux-framework-demo'),
            // 'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            //Must provide key => value pairs for options
            'options' => array(
                '1' => 'ZigZag',
                '2' => 'Default Waves',
                '3' => 'Paper Slides',
                '4' => 'Horizontal Waves',
                '5' => 'Time Machine',
                '6' => 'Silly Waves',

            ),
            // 'required' => array('show-search-btn','equals',true),
            'default' => '2'
        ),

        array(
            'id'        => 'opt-slider-label',
            'type'      => 'slider',
            'title'     => __('Layers', 'redux-framework-demo'),
            'subtitle'  => __('Choose the number of the animation layers.', 'redux-framework-demo'),
            "default"   => 3,
            "min"       => 1,
            "step"      => 1,
            "max"       => 4,
            'display_value' => 'label'
        ),




        array(
            'id' => 'overlay-section-start',
            'type' => 'section',
            'title' => __('Layers colors', 'redux-framework-demo'),
            // 'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
            'indent' => false
        ),


        array(
            'id'       => 'overlay-color-1-primary',
            'type'     => 'switch',
            'title'    => __('Use Primary gradient color for layer 1', 'redux-framework-demo'),
            'desc' => __('You can edit the primary gradient color in the colors section (left menu)', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'overlay-color-1',
            'type'     => 'color_gradient',
            'title'    => __('layer 1 Color', 'redux-framework-demo'),
            // 'subtitle' => __('Only color validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            'validate' => 'color',
            'transparent' => false,
            'default'  => array(
                'from' => '#7d8dff',
                'to'   => '#ff4f81',
            ),
            'required' => array('overlay-color-1-primary','equals',false),
        ),
        array(
            'id'     => 'overlay-section-end',
            'type'   => 'section',
            'indent' => false,
        ),

        array(
            'id'       => 'overlay-color-2',
            'type'     => 'color_gradient',
            'title'    => __('layer 2 Color', 'redux-framework-demo'),
            // 'subtitle' => __('Only color validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            'validate' => 'color',
            'transparent' => false,
            'default'  => array(
                'from' => '#ff4f81',
                'to'   => '#ff4f81',
            ),
            'required' => array('opt-slider-label','>=',2),
        ),
        array(
            'id'       => 'overlay-color-3',
            'type'     => 'color_gradient',
            'title'    => __('layer 3 Color', 'redux-framework-demo'),
            // 'subtitle' => __('Only color validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            'validate' => 'color',
            'transparent' => false,
            'default'  => array(
                'from' => '#7d8dff',
                'to'   => '#7d8dff',
            ),
            'required' => array('opt-slider-label','>=',3),
        ),
        array(
            'id'       => 'overlay-color-4',
            'type'     => 'color_gradient',
            'title'    => __('Layer 4 Color', 'redux-framework-demo'),
            // 'subtitle' => __('Only color validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            'validate' => 'color',
            'transparent' => false,
            'default'  => array(
                'from' => '#7d8dff',
                'to'   => '#ff4f81',
            ),
            'required' => array('opt-slider-label','>=',4),
        ),




    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Social Icons', 'redux-framework-demo' ),
    'id'         => 'social',
    'subsection' => true,
    'fields'     => array(



        array(
            'id'       => 'social-skype',
            'type'     => 'text',
            'title'    => __( 'Skype', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Skype username here', 'redux-framework-demo' ),
            'desc'     => __( 'You can use <strong>callto:</strong> or <strong>skype:</strong> prefix', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-facebook',
            'type'     => 'text',
            'title'    => __( 'Facebook', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Facebook link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-google',
            'type'     => 'text',
            'title'    => __( 'Google', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Google link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-twitter',
            'type'     => 'text',
            'title'    => __( 'Twitter', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Twitter link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-vimeo',
            'type'     => 'text',
            'title'    => __( 'Vimeo', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Vimeo link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-youtube',
            'type'     => 'text',
            'title'    => __( 'YouTube', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your YouTube link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-flickr',
            'type'     => 'text',
            'title'    => __( 'Flickr', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Flickr link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-linkedin',
            'type'     => 'text',
            'title'    => __( 'LinkedIn', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your LinkedIn link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-pinterest',
            'type'     => 'text',
            'title'    => __( 'Pinterest', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your YouTube link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-dribbble',
            'type'     => 'text',
            'title'    => __( 'Dribbble', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Dribbble link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-instagram',
            'type'     => 'text',
            'title'    => __( 'Instagram', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Instagram link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-snapchat',
            'type'     => 'text',
            'title'    => __( 'Snapchat', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Snapchat link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-telegram',
            'type'     => 'text',
            'title'    => __( 'Telegram', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Telegram link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-googleplay',
            'type'     => 'text',
            'title'    => __( 'Google play', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your Google play link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-appstore',
            'type'     => 'text',
            'title'    => __( 'App store', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your App store link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-whatsapp',
            'type'     => 'text',
            'title'    => __( 'Whatsapp', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your whatsapp link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'social-flipboard',
            'type'     => 'text',
            'title'    => __( 'Flipboard', 'redux-framework-demo' ),
            'subtitle' => __( 'Type your flipboard link here', 'redux-framework-demo' ),
            'desc'     => __( 'Icon won`t show if you leave this field blank', 'redux-framework-demo' ),
            'default'  => '',
        ),



    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Colors', 'redux-framework-demo' ),
    'id'         => 'layout-colors',
    'subsection' => true,
    'desc'       => __( 'For detailed information about theme colors please check this article from our knowledge base: ', 'redux-framework-demo' ) . '<a target="_blank" href="https://essentials.pixfort.com/knowledge-base/essentials-color-system" target="_blank">https://essentials.pixfort.com/knowledge-base/essentials-color-system</a>',
    'fields'     => array(
        array(
            'id'       => 'opt-primary-color',
            'type'     => 'color',
            'title'    => __('Primary Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the primary color for the theme.', 'redux-framework-demo'),
            'default'  => '#7d8dff',
            'transparent' => false,
            'validate' => 'color',
        ),

        array(
            'id'       => 'opt-secondary-color',
            'type'     => 'color',
            'title'    => __('Secondary Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the secondary color for the theme.', 'redux-framework-demo'),
            'default'  => '#ff4f81',
            'transparent' => false,
            'validate' => 'color',
        ),



        array(
            'id'       => 'opt-link-color',
            'type'     => 'color',
            'title'    => __('Link Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the link color for the theme.', 'redux-framework-demo'),
            'default'  => '#333333',
            'transparent' => false,
            'validate' => 'color',
        ),

        array(
            'id' => 'section-primary-start',
            'type' => 'section',
            'title' => __('Primary Gradient color', 'redux-framework-demo'),
            // 'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
            'indent' => false
        ),
        array(
            'id'       => 'opt-primary-gradient-switch',
            'type'     => 'switch',
            'title'    => __('Use 3 colors gradient', 'redux-framework-demo'),
            // 'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
            'default'  => false,
        ),
        array(
            'id'       => 'opt-color-gradient-primary-1',
            'type'     => 'color',
            'title'    => __('Gradient start (Left)', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'default'  => '#7d8dff',
            'transparent' => false,
            'validate' => 'color',
        ),
        array(
            'id'       => 'opt-color-gradient-primary-middle',
            'type'     => 'color',
            'title'    => __('Middle Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'default'  => '#4ED199',
            'transparent' => false,
            'required' => array('opt-primary-gradient-switch','equals',true),
            // 'permissions' => 'opt-primary-gradient-switch',
            'validate' => 'color',
        ),
        array(
            'id'       => 'opt-color-gradient-primary-2',
            'type'     => 'color',
            'title'    => __('Gradient end (Right)', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'default'  => '#ff4f81',
            'transparent' => false,
            'validate' => 'color',
        ),

        array(
            'id'       => 'opt-primary-gradient-dir',
            'type'     => 'select',
            'title'    => __('Gradient direction', 'redux-framework-demo'),
            // 'subtitle' => __('Pick the text color.', 'redux-framework-demo'),
            'default'   => "to right",
            'options'  => array(
                "to right"            => "Left to right",
                "to top"        => "Bottom to top",
                "to top right"        => "Bottom left to top right",
                "to bottom right"        => "Top left to bottom right",
            )
        ),

        array(
            'id'     => 'section-primary-end',
            'type'   => 'section',
            'indent' => false,
        ),





    )
) );







Redux::setSection( $opt_name, array(
    'title'      => __( 'Advanced', 'redux-framework-demo' ),
    'id'         => 'layout-advanced',
    'subsection' => true,
    'fields'     => array(


        array(
            'id'       => 'pic-custom-css',
            'type'     => 'ace_editor',
            'title'    => __('Custom CSS', 'redux-framework-demo'),
            // 'subtitle' => __('Paste your CSS code here.', 'redux-framework-demo'),
            'mode'     => 'css',
            'theme'    => 'monokai',
            'desc'     => 'Add custom CSS to your website.',
            'default'  => ""
        ),
        array(
            'id'       => 'pix-custom-js-header',
            'type'     => 'ace_editor',
            'title'    => __('Custom JS (in header)', 'redux-framework-demo'),
            // 'subtitle' => __('Paste your CSS code here.', 'redux-framework-demo'),
            'mode'     => 'js',
            'theme'    => 'monokai',
            'desc'     => 'Add custom JS code to your website header.',
            'default'  => ""
        ),
        array(
            'id'       => 'pix-custom-js-footer',
            'type'     => 'ace_editor',
            'title'    => __('Custom JS (in footer)', 'redux-framework-demo'),
            // 'subtitle' => __('Paste your CSS code here.', 'redux-framework-demo'),
            'mode'     => 'js',
            'theme'    => 'monokai',
            'desc'     => 'Add custom JS code to your website footer.',
            'default'  => ""
        ),

    )
) );









Redux::setSection( $opt_name, array(
    'title' => __( 'Blog', 'redux-framework-demo' ),
    'id'    => 'blog_section',
    // 'desc'  => __( 'Blog & Posts settings.', 'redux-framework-demo' ),
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/blog.svg',
    'icon_type'  => 'svg'
) );





Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    'id'         => 'blog',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'blog-posts',
            'type'     => 'text',
            'title'    => __( 'Items per page', 'redux-framework-demo' ),
            'default'  => '8',
        ),


        array(
            'id'       => 'blog-page-layout',
            'type'     => 'image_select',
            'width'     => '150px',
            'height'     => '100px',
            'title'    => __('Blog page layout', 'redux-framework-demo'),
            'subtitle' => __('Layout for blog posts', 'redux-framework-demo'),
            'options'  => array(
                'default'      => array(
                    'title'   => 'Default',
                    'img'   => ReduxFramework::$_url.'assets/img/options/default-blog-page.jpg'
                ),
                'grid'      => array(
                    'title'   => 'Normal Grid',
                    'img'   => ReduxFramework::$_url.'assets/img/options/grid-blog-page.jpg'
                ),
                'masonry'      => array(
                    'title'   => 'Masonry grid',
                    'img'   => ReduxFramework::$_url.'assets/img/options/masonry-blog-page.jpg'
                ),
            ),
            'default' => 'default',
        ),

        array(
            'id'       => 'blog-grid-count',
            'type'     => 'select',
            'title'    => __('Grid post count per line', 'redux-framework-demo'),
            'options'  => array(
                '6'             => '2',
                '4'             => '3',
                '3'         => '4',
            ),
            'default'  => '4',
            'required' => array('blog-page-layout','=','grid'),
        ),
        array(
            'id'       => 'blog-masonry-count',
            'type'     => 'select',
            'title'    => __('Masonry post count per line', 'redux-framework-demo'),
            'options'  => array(
                '2'             => '2',
                '3'             => '3',
                '4'         => '4',
                '5'         => '5',
            ),
            'default'  => '3',
            'required' => array('blog-page-layout','=','masonry'),
        ),

        array(
            'id'       => 'blog-style',
            'type'     => 'image_select',
            'width'     => '150px',
            'height'     => '100px',
            'title'    => __('Posts Style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for blog posts', 'redux-framework-demo'),
            'options'  => array(
                'default'      => array(
                    'title'   => 'Default',
                    'img'   => ReduxFramework::$_url.'assets/img/options/style-default.jpg'
                ),
                'with-padding'      => array(
                    'title'   => 'With Padding',
                    'img'   => ReduxFramework::$_url.'assets/img/options/style-with-padding.jpg'
                ),
                'left-img'      => array(
                    'title'   => 'Left image',
                    'img'   => ReduxFramework::$_url.'assets/img/options/style-left-image.jpg'
                ),
                'right-img'      => array(
                    'title'   => 'Right image',
                    'img'   => ReduxFramework::$_url.'assets/img/options/style-right-image.jpg'
                ),
                'full-img'      => array(
                    'title'   => 'Full Image',
                    'img'   => ReduxFramework::$_url.'assets/img/options/style-full-image.jpg'
                ),
            ),
            'default' => 'default',
        ),


        array(
            'id'       => 'blog-layout',
            'type'     => 'image_select',
            'width'     => '150px',
            'height'     => '100px',
            'title'    => __('Post layout', 'redux-framework-demo'),
            'subtitle' => __('Layout for blog posts', 'redux-framework-demo'),
            'options'  => array(
                'default'      => array(
                    'title'   => 'Default (Full width)',
                    'img'   => ReduxFramework::$_url.'assets/img/options/default-layout.jpg'
                ),
                'right-sidebar'      => array(
                    'title'   => 'Right sidebar',
                    'img'   => ReduxFramework::$_url.'assets/img/options/sidebar-right.jpg'
                ),
                'left-sidebar'      => array(
                    'title'   => 'Left Sidebar',
                    'img'   => ReduxFramework::$_url.'assets/img/options/sidebar-left.jpg'
                ),
            ),
            'default' => 'default',
        ),
        // array(
        //     'id'       => 'blog-layout',
        //     'type'     => 'select',
        //     'title'    => __('Post layout', 'redux-framework-demo'),
        //     'options'  => array(
        //         'default'             => 'Default (Full width)',
        //         'right-sidebar'         => 'Right sidebar',
        //         'left-sidebar'         => 'Left sidebar',
        //     ),
        //     'default'  => 'default',
        // ),

        array(
            'id'       => 'blog-style-box',
            'type'     => 'switch',
            'title'    => __('Add box style', 'redux-framework-demo'),
            'desc' => __('Add white box for each post.', 'redux-framework-demo'),
            'default'  => true,
            'required' => array('blog-style','!=','full-img'),
        ),

        array(
            'id'       => 'blog-bg-color',
            'type'     => 'select',
            'title'    => __('Background color', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'gray-1',
        ),
        array(
            'id'       => 'custom-blog-bg-color',
            'type'     => 'color',
            'title'    => __('Custom background Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('blog-bg-color','equals','custom'),
            'validate' => 'color',
        ),


        array(
            'id'       => 'sidebar-blog',
            'type'     => 'select',
            'title'    => __('Blog sidebar', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'sidebar-1',
            'options'  => $sidebars
        ),

        array(
            'id'       => 'blog-box-rounded',
            'type'     => 'select',
            'title'    => __('Post rounded corners', 'redux-framework-demo'),
            'default'  => 'rounded-lg',
            // 'required' => array('post-with-intro','!=',false),
            'options'  => array(
                'rounded-0'   => "No",
                'rounded'   => "Rounded",
                'rounded-lg'   => "Rounded Large",
                'rounded-xl'   => "Rounded 5px",
                'rounded-10'   => "Rounded 10px"
            )
        ),
        array(
            'id'       => 'blog-box-style',
            'type'     => 'select',
            'title'    => __('Post shadow Style', 'redux-framework-demo'),
            'default'  => '1',
            // 'required' => array('post-with-intro','!=',false),
            'options'  => array(
                "" => "Default",
                "1"       => "Small shadow",
                "2"       => "Medium shadow",
                "3"       => "Large shadow",
                "4"       => "Inverse Small shadow",
                "5"       => "Inverse Medium shadow",
                "6"       => "Inverse Large shadow",
            )
        ),
        array(
            'id'       => 'blog-box-hover-effect',
            'type'     => 'select',
            'title'    => __('Post Shadow Hover Style', 'redux-framework-demo'),
            'default'  => '1',
            // 'required' => array('post-with-intro','!=',false),
            'options'  => array(
                ""       => "None",
                "1"       => "Small hover shadow",
                "2"       => "Medium hover shadow",
                "3"       => "Large hover shadow",
                "4"       => "Inverse Small hover shadow",
                "5"       => "Inverse Medium hover shadow",
                "6"       => "Inverse Large hover shadow",
            )
        ),
        array(
            'id'       => 'blog-box-add-hover-effect',
            'type'     => 'select',
            'title'    => __('Post Hover Animation', 'redux-framework-demo'),
            'default'  => '1',
            // 'required' => array('post-with-intro','!=',false),
            'options'  => array(
                ""       => "None",
                "1"       => "Fly Small",
                "2"       => "Fly Medium",
                "3"       => "Fly Large",
                "4"       => "Scale Small",
                "5"       => "Scale Medium",
                "6"       => "Scale Large",
                "7"       => "Scale Inverse Small",
                "8"       => "Scale Inverse Medium",
                "9"       => "Scale Inverse Large",
            )
        ),


    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Intro', 'redux-framework-demo' ),
    'id'         => 'blog_intro',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'post-with-intro',
            'type'     => 'switch',
            'title'    => __('Enable post intro', 'redux-framework-demo'),
            'desc' => __('Add intro section at the beginning of the post.', 'redux-framework-demo'),
            'default'  => true,
        ),


        array(
            'id'       => 'blog-divider-style',
            'type'     => 'image_select',
            'class'    => 'pix-opts-dividers',
            'title'    => __('Divider style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for portfolio items list', 'redux-framework-demo'),
            //'desc' => __('This option can <strong>not</strong> be overriden and it is usefull for people who already have many posts and want to standardize their appearance.', 'redux-framework-demo'),
            'options'  => $opts_dividers,
            'default' => '0',
            'required' => array('post-with-intro','!=',false),
        ),

        array(
            'id'       => 'blog-divider-height',
            'type'     => 'text',
            'title'    => __( 'Custom Divider height (Optional)', 'redux-framework-demo' ),
            'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            'default'  => '',
            'required' => array('post-with-intro','!=',false),
        ),

        array(
            'id'       => 'blog-intro-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Intro background image', 'redux-framework-demo'),
            'required' => array('post-with-intro','!=',false),
        ),
        array(
            'id'       => 'blog-intro-light',
            'type'     => 'switch',
            'title'    => __('Enable light intro text', 'redux-framework-demo'),
            'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => true,
            'required' => array('post-with-intro','!=',false),
        ),

        array(
            'id'       => 'blog-intro-align',
            'type'     => 'select',
            'title'    => __('Intro text align', 'redux-framework-demo'),
            'default'  => 'text-center',
            'required' => array('post-with-intro','!=',false),
            'options'  => array(
                'text-left'   => "Left",
                'text-center'   => "Center",
                'text-right'   => "Right"
            )
        ),


        array(
            'id'       => 'blog-intr-bg-color',
            'type'     => 'select',
            'title'    => __('Intro overlay color', 'redux-framework-demo'),
            'default'  => 'primary',
            'options'  => array_flip($colors),
            'required' => array('post-with-intro','!=',false),
        ),
        array(
            'id'       => 'blog-intro-opacity',
            'type'     => 'select',
            'title'    => __('Intro overlay opacity', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'pix-opacity-2',
            'options'  => array(
                'pix-opacity-10'   => "0%",
                'pix-opacity-9'   => "10%",
                'pix-opacity-8'   => "20%",
                'pix-opacity-7'   => "30%",
                'pix-opacity-6'   => "40%",
                'pix-opacity-5'   => "50%",
                'pix-opacity-4'   => "60%",
                'pix-opacity-3'   => "70%",
                'pix-opacity-2'   => "80%",
                'pix-opacity-1'   => "90%",
                'pix-opacity-0'   => "100%",
            ),
            'required' => array('post-with-intro','!=',false),
        ),



    )
) );



Redux::setSection( $opt_name, array(
    'title'      => __( 'Advanced', 'redux-framework-demo' ),
    'id'         => 'blog_advanced',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'pix-disable-blog-author-box',
            'type'     => 'switch',
            'title'    => __('Disable post author box', 'redux-framework-demo'),
            // 'desc' => __('If you disable pixfort demo block from Elementor library you will see the default Elementor blocks instead.', 'redux-framework-demo'),
            'default'  => false,
        ),
        array(
            'id'       => 'pix-disable-blog-social',
            'type'     => 'switch',
            'title'    => __('Disable post social share buttons', 'redux-framework-demo'),
            // 'desc' => __('If you disable pixfort demo block from Elementor library you will see the default Elementor blocks instead.', 'redux-framework-demo'),
            'default'  => false,
        ),
        array(
            'id'       => 'pix-disable-blog-related',
            'type'     => 'switch',
            'title'    => __('Disable related posts', 'redux-framework-demo'),
            // 'desc' => __('If you disable pixfort demo block from Elementor library you will see the default Elementor blocks instead.', 'redux-framework-demo'),
            'default'  => false,
        ),


    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Portfolio', 'redux-framework-demo' ),
    'id'    => 'portfolio_section',
    // 'desc'  => __( 'Blog & Posts settings.', 'redux-framework-demo' ),
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/portfolio.svg',
    'icon_type'  => 'svg'
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    'id'         => 'portfolio',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'portfolio-posts',
            'type'     => 'text',
            'title'    => __( 'Items per page', 'redux-framework-demo' ),
            'default'  => '8',
        ),
        array(
            'id'       => 'portfolio-page-style',
            'type'     => 'image_select',
            'width'     => '150px',
            'height'     => '100px',
            'title'    => __('Layout', 'redux-framework-demo'),
            'subtitle' => __('Layout for portfolio items list', 'redux-framework-demo'),
            //'desc' => __('This option can <strong>not</strong> be overriden and it is usefull for people who already have many posts and want to standardize their appearance.', 'redux-framework-demo'),
            'options'  => array(
                'default'      => array(
                    'title'   => 'Default',
                    'img'   => ReduxFramework::$_url.'assets/img/options/portfolio/portfolio-default.png'
                ),
                'mini'      => array(
                    'title'   => 'Mini',
                    'img'   => ReduxFramework::$_url.'assets/img/options/portfolio/portfolio-mini.png'
                ),
                'transparent'      => array(
                    'title'   => 'Transparent',
                    'img'   => ReduxFramework::$_url.'assets/img/options/portfolio/portfolio-transparent.png'
                ),
                '3d'      => array(
                    'title'   => '3D',
                    'img'   => ReduxFramework::$_url.'assets/img/options/portfolio/portfolio-3d.png'
                )
            ),
            'default' => 'default',
        ),



        array(
            'id'       => 'portfolio-masonry-count',
            'type'     => 'select',
            'title'    => __('Masonry portfolio count per line', 'redux-framework-demo'),
            'options'  => array(
                '6'             => '2',
                '4'             => '3',
                '3'         => '4',
                '2'         => '6',
            ),
            'default'  => '4',
            // 'required' => array('portfolio-page-layout','=','masonry'),
        ),

        array(
            'id'       => 'portfolio-layout',
            'type'     => 'select',
            'title'    => __('Layout', 'redux-framework-demo'),
            // 'subtitle' => __('Portfolio items order by column', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array(
                'default'        => 'Default',
                'sidebar'        => 'With Sidebar',
                'sidebar-full'        => 'With Sidebar (Full width)',
                'box'        => 'Intro box',
                // 'content-box'        => 'Boxed content',
            ),
            'default'  => 'default',
        ),

        array(
            'id'       => 'portfolio-order',
            'type'     => 'select',
            'title'    => __('Order', 'redux-framework-demo'),
            'subtitle' => __('Portfolio items order', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array(
                'ASC'   => 'Ascending',
                'DESC'  => 'Descending'
            ),
            'default'  => 'DESC',
        ),
        array(
            'id'       => 'portfolio-navigation',
            'type'     => 'switch',
            'title'    => __('Enable Navigation between projects', 'redux-framework-demo'),
            // 'desc' => __('Navigation arrows refer to projects in the same category', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'portfolio-in-same-term',
            'type'     => 'switch',
            'title'    => __('In same category', 'redux-framework-demo'),
            'desc' => __('Navigation arrows refer to projects in the same category', 'redux-framework-demo'),
            'default'  => false,
        ),
        array(
            'id'       => 'portfolio-post-info',
            'type'     => 'switch',
            'title'    => __('Show date and author info', 'redux-framework-demo'),
            // 'desc' => __('Navigation arrows refer to projects in the same category', 'redux-framework-demo'),
            'default'  => false,
        ),

        array(
            'id'       => 'portfolio-related',
            'type'     => 'switch',
            'title'    => __('Related Projects', 'redux-framework-demo'),
            'desc' => __('Show Related Projects', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'portfolio-isotope',
            'type'     => 'switch',
            'title'    => __('jQuery filtering', 'redux-framework-demo'),
            'desc' => __('When this option is enabled, portfolio looks great with all projects on single site, so please set "Posts per page" option to bigger number', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'portfolio-slug',
            'type'     => 'text',
            'title'    => __( 'Single item slug', 'redux-framework-demo' ),
            'desc' => __('<b>Important:</b> Do not use characters not allowed in links. <br /><br />Must be different from the Portfolio site title chosen above, eg. "portfolio-item". After change please go to "Settings > Permalinks" and click "Save changes" button.', 'redux-framework-demo'),
            'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => 'portfolio-item',
        ),



        array(
            'id'       => 'portfolio-bg-color',
            'type'     => 'select',
            'title'    => __('Background color', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'gray-1',
        ),
        array(
            'id'       => 'custom-portfolio-bg-color',
            'type'     => 'color',
            'title'    => __('Custom background Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('portfolio-bg-color','equals','custom'),
            'validate' => 'color',
        ),

        array(
            'id'       => 'sidebar-portfolio',
            'type'     => 'select',
            'title'    => __('Portfolio sidebar', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'sidebar-1',
            'options'  => $sidebars
        ),

        array(
            'id'       => 'portfolio-orderby',
            'type'     => 'select',
            'title'    => __('Order by', 'redux-framework-demo'),
            'subtitle' => __('Portfolio items order by column', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array(
                'date'        => 'Date',
                'menu_order'        => 'Menu order',
                'title'        => 'Title',
                'rand'        => 'Random',
            ),
            'default'  => 'date',
        ),

    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Intro', 'redux-framework-demo' ),
    'id'         => 'portfolio_intro',
    'subsection' => true,
    'fields'     => array(


        array(
            'id'       => 'portfolio-with-intro',
            'type'     => 'switch',
            'title'    => __('Enable portfolio intro', 'redux-framework-demo'),
            'desc' => __('Add intro section at the beginning of the portfolio.', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'portfolio-divider-style',
            'type'     => 'image_select',
            'class'    => 'pix-opts-dividers',
            'title'    => __('Divider style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for portfolio items list', 'redux-framework-demo'),
            //'desc' => __('This option can <strong>not</strong> be overriden and it is usefull for people who already have many posts and want to standardize their appearance.', 'redux-framework-demo'),
            'options'  => $opts_dividers,
            'default' => '0',
        ),

        array(
            'id'       => 'portfolio-divider-height',
            'type'     => 'text',
            'title'    => __( 'Custom Divider height (Optional)', 'redux-framework-demo' ),
            'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => '',
        ),

        array(
            'id'       => 'portfolio-intro-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Intro background image', 'redux-framework-demo'),
        ),
        array(
            'id'       => 'portfolio-intro-light',
            'type'     => 'switch',
            'title'    => __('Enable light intro text', 'redux-framework-demo'),
            'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'portfolio-intro-align',
            'type'     => 'select',
            'title'    => __('Intro text align', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'text-center',
            'options'  => array(
                'text-left'   => "Left",
                'text-center'   => "Center",
                'text-right'   => "Right"
            )
        ),


        array(
            'id'       => 'portfolio-intr-bg-color',
            'type'     => 'select',
            'title'    => __('Intro overlay color', 'redux-framework-demo'),
            'default'  => 'primary',
            'options'  => array_flip($colors),
        ),
        array(
            'id'       => 'portfolio-intro-opacity',
            'type'     => 'select',
            'title'    => __('Intro overlay opacity', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'pix-opacity-2',
            'options'  => array(
                'pix-opacity-10'   => "0%",
                'pix-opacity-9'   => "10%",
                'pix-opacity-8'   => "20%",
                'pix-opacity-7'   => "30%",
                'pix-opacity-6'   => "40%",
                'pix-opacity-5'   => "50%",
                'pix-opacity-4'   => "60%",
                'pix-opacity-3'   => "70%",
                'pix-opacity-2'   => "80%",
                'pix-opacity-1'   => "90%",
                'pix-opacity-0'   => "100%",
            )
        ),





    )
) );





Redux::setSection( $opt_name, array(
    'title' => __( 'Pages', 'redux-framework-demo' ),
    'id'    => 'pages_section',
    // 'desc'  => __( 'Blog & Posts settings.', 'redux-framework-demo' ),
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/pages.svg',
    'icon_type'  => 'svg'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    'id'         => 'pages',
    'subsection' => true,
    'fields'     => array(




        array(
            'id'       => 'pages-bg-color',
            'type'     => 'select',
            'title'    => __('Page background color', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'gray-1',
        ),
        array(
            'id'       => 'custom-pages-bg-color',
            'type'     => 'color',
            'title'    => __('Custom background Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('pages-bg-color','equals','custom'),
            'validate' => 'color',
        ),

        array(
            'id'       => 'sidebar-page',
            'type'     => 'select',
            'title'    => __('Pages sidebar', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'sidebar-1',
            'options'  => $sidebars
        ),

        array(
            'id'       => 'sidebar-page-sticky',
            'type'     => 'select',
            'title'    => __('Sidebar Sticky', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'sticky-bottom',
            'options'  => array(
                'sticky-bottom'   => "Sticky bottom",
                'sticky-top'   => "Sticky Top",
                'sticky-disabled'   => "Disable Sticky"
            )
        ),

    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Intro', 'redux-framework-demo' ),
    'id'         => 'pages_intro',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'pages-with-intro',
            'type'     => 'switch',
            'title'    => __('Enable pages intro', 'redux-framework-demo'),
            'desc' => __('Add intro section at the beginning of the pages.', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'pages-divider-style',
            'type'     => 'image_select',
            'class'    => 'pix-opts-dividers',
            'title'    => __('Divider style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for portfolio items list', 'redux-framework-demo'),
            //'desc' => __('This option can <strong>not</strong> be overriden and it is usefull for people who already have many posts and want to standardize their appearance.', 'redux-framework-demo'),
            'options'  => $opts_dividers,
            'default' => '0',
        ),

        array(
            'id'       => 'pages-divider-height',
            'type'     => 'text',
            'title'    => __( 'Custom Divider height (Optional)', 'redux-framework-demo' ),
            'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            'default'  => '',
            'required' => array('pages-divider-style','!=','0'),
        ),

        array(
            'id'       => 'pages-intro-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Intro background image', 'redux-framework-demo'),
        ),
        array(
            'id'       => 'pages-intro-light',
            'type'     => 'switch',
            'title'    => __('Enable light intro text', 'redux-framework-demo'),
            'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'pages-intro-align',
            'type'     => 'select',
            'title'    => __('Intro text align', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'text-center',
            'options'  => array(
                'text-left'   => "Left",
                'text-center'   => "Center",
                'text-right'   => "Right"
            )
        ),


        array(
            'id'       => 'pages-intr-bg-color',
            'type'     => 'select',
            'title'    => __('Intro overlay color', 'redux-framework-demo'),
            'default'  => 'primary',
            'options'  => array_flip($colors),
        ),
        array(
            'id'       => 'pages-intro-opacity',
            'type'     => 'select',
            'title'    => __('Intro overlay opacity', 'redux-framework-demo'),
            'default'  => 'pix-opacity-2',
            'options'  => array(
                'pix-opacity-10'   => "0%",
                'pix-opacity-9'   => "10%",
                'pix-opacity-8'   => "20%",
                'pix-opacity-7'   => "30%",
                'pix-opacity-6'   => "40%",
                'pix-opacity-5'   => "50%",
                'pix-opacity-4'   => "60%",
                'pix-opacity-3'   => "70%",
                'pix-opacity-2'   => "80%",
                'pix-opacity-1'   => "90%",
                'pix-opacity-0'   => "100%",
            )
        ),

    )
) );



Redux::setSection( $opt_name, array(
    'title' => __( 'Typography', 'redux-framework-demo' ),
    'id'    => 'typography',
    'desc'  => __( 'Basic fields as subsections.', 'redux-framework-demo' ),
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/typography.svg',
    'icon_type'  => 'svg'
) );




$pix_fonts = array(
    "Arial, Helvetica, sans-serif" => "Arial, Helvetica, sans-serif",
    "'Arial Black', Gadget, sans-serif" => "'Arial Black', Gadget, sans-serif",
    "'Bookman Old Style', serif" => "'Bookman Old Style', serif",
    "'Comic Sans MS', cursive" => "'Comic Sans MS', cursive",
    "Courier, monospace" => "Courier, monospace",
    "Garamond, serif" => "Garamond, serif",
    "Georgia, serif" => "Georgia, serif",
    "Impact, Charcoal, sans-serif" => "Impact, Charcoal, sans-serif",
    "'Lucida Console', Monaco, monospace" => "'Lucida Console', Monaco, monospace",
    "'Lucida Sans Unicode', 'Lucida Grande', sans-serif" => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
    "'MS Sans Serif', Geneva, sans-serif" => "'MS Sans Serif', Geneva, sans-serif",
    "'MS Serif', 'New York', sans-serif" =>"'MS Serif', 'New York', sans-serif",
    "'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
    "Tahoma, Geneva, sans-serif" =>"Tahoma, Geneva, sans-serif",
    "'Times New Roman', Times, serif" => "'Times New Roman', Times, serif",
    "'Trebuchet MS', Helvetica, sans-serif" => "'Trebuchet MS', Helvetica, sans-serif",
    "Verdana, Geneva, sans-serif" => "Verdana, Geneva, sans-serif",
);

if(!empty(pix_plugin_get_option('opt-external-font-1-url'))){
    $pix_fonts[pix_plugin_get_option('opt-external-font-1-name')] = 'pix-'.pix_plugin_get_option('opt-external-font-1-name');
}
if(!empty(pix_plugin_get_option('opt-external-font-2-name'))){
    $pix_fonts[pix_plugin_get_option('opt-external-font-2-name')] = 'pix-'.pix_plugin_get_option('opt-external-font-2-name');
}


Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    'id'         => 'typography-general',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'opt-primary-font',
            'type'        => 'typography',
            'title'       => __('Body font', 'redux-framework-demo'),
            'google'      => true,
            'font-backup' => true,
            // 'fonts' => array('verdana,san-serif,helvatica' => 'Verdana'),
            // 'output'      => array('body'),
            'output'      => false,
            'fonts' => $pix_fonts,
            'units'       =>'px',
            'color'       => false,
            'font-style'  => false,
            'font-size'   => false,
            'line-height' => false,
            'text-align' => false,
            // 'word-spacing' => true,
            // 'letter-spacing' => true,
            // 'font-weight' => '400,700,900',
            'font-weight' => false,
            'subsets' => false,
            'desc'    => __('The font family for body texts and paragraphs.', 'redux-framework-demo'),
            'default'     => array(
                'font-family' => 'Manrope',
                'google'      => true,
                // 'letter-spacing' => '-0.1'
            ),
        ),
        array(
            'id'       => 'opt-primary-font-spacing',
            'type'     => 'text',
            'title'    => __( 'Primary font spacing', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Type your LinkedIn link here', 'redux-framework-demo' ),
            'desc'     => __( 'Specify the spacing between the letters (for example: -0.01em)', 'redux-framework-demo' ),
            'default'  => '-0.01em',
        ),
        array(
            'id'          => 'opt-secondary-font',
            'type'        => 'typography',
            'title'       => __('Heading font', 'redux-framework-demo'),
            'subtitle' => __( 'Secondary font for headings', 'redux-framework-demo' ),
            'google'      => true,
            'font-backup' => true,
            // 'output'      => array('.secondary-font'),
            'output'      => false,
            'units'       =>'px',
            'color'       => false,
            'fonts'       => $pix_fonts,
            'font-style'  => false,
            'font-size'   => false,
            'line-height' => false,
            'text-align' => false,
            'font-weight' => false,
            // 'letter-spacing' => true,
            'subsets' => false,
            'desc'    => __('The font family for headings (H1, H2, H3, H4, H5, H6).', 'redux-framework-demo'),
            'default'     => array(
                'font-family' => 'Manrope',
                'google'      => true,
                // 'letter-spacing' => '-0.1'
            ),
        ),
        array(
            'id'       => 'opt-secondary-font-spacing',
            'type'     => 'text',
            'title'    => __( 'Heading font spacing', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Type your LinkedIn link here', 'redux-framework-demo' ),
            'desc'     => __( 'Specify the spacing between the letters (for example: -0.01em)', 'redux-framework-demo' ),
            'default'  => '-0.01em',
        ),



        array(
            'id'       => 'opt-body-color',
            'type'     => 'select',
            'title'    => __('Body Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the body text color for light backgrounds.', 'redux-framework-demo'),
            // 'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array_flip($colors),
            'default'  => 'gray-5',

        ),

        array(
            'id'       => 'opt-custom-body-color',
            'type'     => 'color',
            'title'    => __('Custom Body Color', 'redux-framework-demo'),
            'default'  => '#212529',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('opt-body-color','=','custom'),
        ),

        array(
            'id'       => 'opt-dark-body-color',
            'type'     => 'select',
            'title'    => __('Dark Body Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the body text color for dark backgrounds.', 'redux-framework-demo'),
            // 'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array_flip($colors),
            'default'  => 'light-opacity-7',

        ),

        array(
            'id'       => 'opt-custom-dark-body-color',
            'type'     => 'color',
            'title'    => __('Custom Dark Body Color', 'redux-framework-demo'),
            'default'  => '#eee',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('opt-dark-body-color','=','custom'),
        ),



        array(
            'id'       => 'opt-heading-color',
            'type'     => 'select',
            'title'    => __('Heading Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the heading text color for light backgrounds.', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'gray-7',

        ),

        array(
            'id'       => 'opt-custom-heading-color',
            'type'     => 'color',
            'title'    => __('Custom Heading Color', 'redux-framework-demo'),
            'default'  => '#eee',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('opt-heading-color','=','custom'),
        ),
        array(
            'id'       => 'opt-dark-heading-color',
            'type'     => 'select',
            'title'    => __('Dark Heading Color', 'redux-framework-demo'),
            'subtitle' => __('Pick the heading text color for dark backgrounds.', 'redux-framework-demo'),
            'options'  => array_flip($colors),
            'default'  => 'white',

        ),

        array(
            'id'       => 'opt-custom-dark-heading-color',
            'type'     => 'color',
            'title'    => __('Custom Dark Heading Color', 'redux-framework-demo'),
            'default'  => '#fff',
            'transparent' => false,
            'validate' => 'color',
            'required' => array('opt-dark-heading-color','=','custom'),
        ),

        array(
            'id'       => 'opt-regular-font-weight',
            'type'     => 'text',
            'title'    => __( 'Normal text font weight', 'redux-framework-demo' ),
            'desc'     => __( 'default regualr font weight is 400', 'redux-framework-demo' ),
            'default'   => ''
        ),
        array(
            'id'       => 'opt-bold-font-weight',
            'type'     => 'text',
            'title'    => __( 'Bold font weight', 'redux-framework-demo' ),
            'desc'     => __( 'default bold font weight is 700', 'redux-framework-demo' ),
            'default'   => ''
        ),

    )
)
);

Redux::setSection( $opt_name, array(
    'title'      => __( 'Advanced', 'redux-framework-demo' ),
    'desc'       => __( 'For detailed information about using external font please check this article from our knowledge base: ', 'redux-framework-demo' ) . '<a target="_blank" href="https://essentials.pixfort.com/knowledge-base/how-to-use-external-fonts/" target="_blank">https://essentials.pixfort.com/knowledge-base/how-to-use-external-fonts/</a>',
    'id'         => 'opt-textarea-subsection',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'opt-external-font-1-url',
            'type'     => 'text',
            'title'    => __( 'External font 1 link', 'redux-framework-demo' ),
            'desc'     => __( 'Add the link of the external font', 'redux-framework-demo' ),
        ),
        array(
            'id'       => 'opt-external-font-1-name',
            'type'     => 'text',
            'title'    => __( 'External font 1 Name', 'redux-framework-demo' ),
            'desc'     => __( 'Add the name of the external font', 'redux-framework-demo' ),
        ),
        array(
            'id'       => 'opt-external-font-2-url',
            'type'     => 'text',
            'title'    => __( 'External font 2 link', 'redux-framework-demo' ),
            'desc'     => __( 'Add the link of the external font', 'redux-framework-demo' ),
        ),
        array(
            'id'       => 'opt-external-font-2-name',
            'type'     => 'text',
            'title'    => __( 'External font 2 Name', 'redux-framework-demo' ),
            'desc'     => __( 'Add the name of the external font', 'redux-framework-demo' ),
        ),

    )
) );









Redux::setSection( $opt_name, array(
    'title' => __( 'Shop', 'redux-framework-demo' ),
    'id'    => 'shop',
    'desc'  => __( 'Woocommerce shop settings.', 'redux-framework-demo' ),
    'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/shop.svg',
    'icon_type'  => 'svg'
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'General', 'redux-framework-demo' ),
    // 'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/textarea/" target="_blank">http://docs.reduxframework.com/core/fields/textarea/</a>',
    'id'         => 'opt-shop-advanced',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'shop-layout',
            'type'     => 'select',
            'title'    => __('Shop page layout', 'redux-framework-demo'),
            'default'  => 'right-sidebar',
            'options'  => array(
                'right-sidebar'   => "Right sidebar",
                'left-sidebar'   => "Left sidebar",
                'no-sidebar'   => "No sidebar",
            )
        ),
        // array(
        //     'id'       => 'shop-item-style',
        //     'type'     => 'select',
        //     'title'    => __('Shop item style', 'redux-framework-demo'),
        //     'default'  => 'default',
        //     'options'  => array(
        //         'default'   => "Default",
        //         'default-no-padding'   => "Default no padding",
        //         'top-img'   => "Top image",
        //         'top-img-no-padding'   => "Top image no padding",
        //         'full-img'   => "Full image",
        //     )
        // ),


        array(
            'id'       => 'shop-item-style',
            'type'     => 'image_select',
            'width'     => '150px',
            'height'     => '100px',
            'title'    => __('Shop item style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for blog posts', 'redux-framework-demo'),
            'options'  => array(
                'default'      => array(
                    'title'   => 'Default',
                    'img'   => ReduxFramework::$_url.'assets/img/options/shop/shop-default.png'
                ),
                'default-no-padding'      => array(
                    'title'   => 'Default no padding',
                    'img'   => ReduxFramework::$_url.'assets/img/options/shop/shop-default-no-padding.png'
                ),
                'top-img'      => array(
                    'title'   => 'Top image',
                    'img'   => ReduxFramework::$_url.'assets/img/options/shop/shop-top-image.png'
                ),
                'top-img-no-padding'      => array(
                    'title'   => 'Top image no padding',
                    'img'   => ReduxFramework::$_url.'assets/img/options/shop/shop-top-image-no-padding.png'
                ),
                'full-img'      => array(
                    'title'   => 'Full image',
                    'img'   => ReduxFramework::$_url.'assets/img/options/shop/shop-full-image.png'
                ),
            ),
            'default' => 'default',
        ),

        array(
            'id'       => 'shop-col-count',
            'type'     => 'select',
            'title'    => __('Number of columns in shop page', 'redux-framework-demo'),
            'default'  => '3',
            'options'  => array(
                '2'		=> '2',
                '3'		=> '3 (Default)',
                '4'		=> '4',
                '5'		=> '5',
            )
        ),


        array(
            'id'       => 'shop-tabs-style',
            'type'     => 'select',
            'title'    => __('Shop tabs style', 'redux-framework-demo'),
            'default'  => 'pix-pills-1',
            'options'  => array(
                'pix-pills-1'		=> 'Default (Gradient)',
                'pix-pills-solid'			=> 'Solid',
                'pix-pills-light'			=> 'Light',
                'pix-pills-outline'			=> 'Outline',
                'pix-pills-line'			=> 'Line',
                'pix-pills-round'			=> 'Round',
            )
        ),


        array(
            'id'       => 'shop-single-layout',
            'type'     => 'select',
            'title'    => __('Product page layout', 'redux-framework-demo'),
            'default'  => 'default',
            'options'  => array(
                'default'		=> 'Default (Gallery)',
                'pix-boxed-2'		=> 'Gallery - Boxed description',
                'layout-2'			=> 'Full',
                'layout-3'			=> 'Full - Boxed description',
            )
        ),
        array(
            'id'       => 'shop-single-sidebar',
            'type'     => 'switch',
            'title'    => __('Enable sidebar in product page', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => false,
        ),


        array(
            'id'       => 'shop-bg-color',
            'type'     => 'select',
            'title'    => __('Background color', 'redux-framework-demo'),
            // 'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
            // 'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            // Must provide key => value pairs for select options
            'options'  => array_flip($colors),
            'default'  => 'gray-1',
        ),



        array(
            'id'       => 'custom-shop-bg-color',
            'type'     => 'color',
            'title'    => __('Custom background Color', 'redux-framework-demo'),
            // 'subtitle' => __('Pick a background color for the theme (default: #fff).', 'redux-framework-demo'),
            'transparent' => false,
            'default'  => '#FFFFFF',
            'required' => array('shop-bg-color','equals','custom'),
            'validate' => 'color',
        ),


        array(
            'id'       => 'sidebar-shop',
            'type'     => 'select',
            'title'    => __('Shop sidebar', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'sidebar-1',
            'options'  => $sidebars
        ),


    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Intro', 'redux-framework-demo' ),
    'id'         => 'shop-general',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'shop-with-intro',
            'type'     => 'switch',
            'title'    => __('Enable shop intro', 'redux-framework-demo'),
            'desc' => __('Add intro section at the beginning of the shop.', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'shop-divider-style',
            'type'     => 'image_select',
            'class'    => 'pix-opts-dividers',
            'title'    => __('Divider style', 'redux-framework-demo'),
            // 'subtitle' => __('Layout for portfolio items list', 'redux-framework-demo'),
            //'desc' => __('This option can <strong>not</strong> be overriden and it is usefull for people who already have many posts and want to standardize their appearance.', 'redux-framework-demo'),
            'options'  => $opts_dividers,
            'default' => '0',
        ),

        array(
            'id'       => 'shop-divider-height',
            'type'     => 'text',
            'title'    => __( 'Custom Divider height (Optional)', 'redux-framework-demo' ),
            'desc' => __('Leave empty to use the default height for each divider.', 'redux-framework-demo'),
            // 'subtitle' => __('Link to single item', 'redux-framework-demo'),
            'default'  => '',
        ),

        array(
            'id'       => 'shop-intro-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Intro background image', 'redux-framework-demo'),
        ),
        array(
            'id'       => 'shop-intro-light',
            'type'     => 'switch',
            'title'    => __('Enable light intro text', 'redux-framework-demo'),
            'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => true,
        ),

        array(
            'id'       => 'shop-intro-align',
            'type'     => 'select',
            'title'    => __('Intro text align', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'text-center',
            'options'  => array(
                'text-left'   => "Left",
                'text-center'   => "Center",
                'text-right'   => "Right"
            )
        ),



        array(
            'id'       => 'shop-intr-bg-color',
            'type'     => 'select',
            'title'    => __('Intro overlay color', 'redux-framework-demo'),
            'default'  => 'primary',
            'options'  => array_flip($colors),
        ),
        array(
            'id'       => 'shop-intro-opacity',
            'type'     => 'select',
            'title'    => __('Intro overlay opacity', 'redux-framework-demo'),
            // 'desc' => __('Disable to display dark text in the intro.', 'redux-framework-demo'),
            'default'  => 'pix-opacity-2',
            'options'  => array(
                'pix-opacity-10'   => "0%",
                'pix-opacity-9'   => "10%",
                'pix-opacity-8'   => "20%",
                'pix-opacity-7'   => "30%",
                'pix-opacity-6'   => "40%",
                'pix-opacity-5'   => "50%",
                'pix-opacity-4'   => "60%",
                'pix-opacity-3'   => "70%",
                'pix-opacity-2'   => "80%",
                'pix-opacity-1'   => "90%",
                'pix-opacity-0'   => "100%",
            )
        ),





    )
)
);

// Redux::setSection( $opt_name, array(
//     'title' => __( 'Activation', 'redux-framework-demo' ),
//     'id'    => 'activation',
//     'desc'  => __( 'Theme activation.', 'redux-framework-demo' ),
//     'icon'  => PIX_CORE_PLUGIN_DIR.'/functions/images/options/shop.svg',
//     'icon_type'  => 'svg',
//     'fields'     => array(
//
//         array(
//             'id'       => 'shop-col-count2',
//             'type'     => 'select',
//             'title'    => __('Number of columns in shop page', 'redux-framework-demo'),
//             'default'  => '3',
//             'options'  => array(
//                 '2'		=> '2',
//                 '3'		=> '3 (Default)',
//                 '4'		=> '4',
//                 '5'		=> '5',
//             )
//         ),
//
//     )
// ) );
//
// Redux::setSection( $opt_name, array(
//     'title'      => __( 'General', 'redux-framework-demo' ),
//     'id'         => 'shop-general',
//     'subsection' => true,
//     'fields'     => array(
//
//         array(
//             'id'       => 'shop-col-count2',
//             'type'     => 'select',
//             'title'    => __('Number of columns in shop page', 'redux-framework-demo'),
//             'default'  => '3',
//             'options'  => array(
//                 '2'		=> '2',
//                 '3'		=> '3 (Default)',
//                 '4'		=> '4',
//                 '5'		=> '5',
//             )
//         ),
//
//     )
// )
// );


function addAndOverridePanelCSS() {
    // wp_dequeue_style( 'redux-admin-css' );
    wp_register_style(
        'redux-custom-css',
        PIX_CORE_PLUGIN_URI.'functions/css/admin-panel.css',
        array( 'farbtastic' ), // Notice redux-admin-css is removed and the wordpress standard farbtastic is included instead
        time(),
        'all'
    );
    wp_enqueue_style('redux-custom-css');
}
// This example assumes your opt_name is set to redux_demo, replace with your opt_name value
add_action( 'redux/page/pix_options/enqueue', 'addAndOverridePanelCSS' );




?>
